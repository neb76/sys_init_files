execute pathogen#infect()
syntax on
filetype plugin indent on
set background=dark
colorscheme solarized
set number
vnoremap <C-c> "*y
