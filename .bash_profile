export PATH=/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin
alias ATLAS='ssh -X neb76@atlas.cac.cornell.edu'
FROM_ATLAS='scp -r neb76@atlas.cac.cornell.edu:'
alias COLUMBA='ssh -X neb76@132.236.164.129'
FROM_COLUMBA= 'scp -r neb76@132.236.164.129:'
TO_COLUMBA_home= 'neb76@132.236.164.129:/~'

alias START_DAY='cd /Users/nicholasbruns/repos/work_repos/stem_abund/'
alias HOUSE_CLEAN='cd /Users/nicholasbruns/Desktop/December_house.clean'
alias SNIPS='cd ~/Library/Application\ Support/Sublime\ Text\ 2/Packages/User/user_snippets/R/'
export PATH=/usr/texbin:$PATH
shopt -s cdspell
set -o vi
